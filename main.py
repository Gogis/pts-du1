import sys
from math import floor


class Player:

    """
    Player class which stores name, number of points
    and junior status of each registered player

    Attributes:
        name (str): Name of a player
        points (int): Number of a player's points
        junior (boolean): Player's junior status
    """
    def __init__(self, name, points=0, junior=False):
        self.name = name
        self.points = points
        self.junior = junior

    def name(self):
        return self.name

    @property
    def points(self):
        return self._points

    @points.setter
    def points(self, points):
        self._points = points

    @property
    def junior(self):
        return self._junior

    @junior.setter
    def junior(self, junior):
        self._junior = junior


my_password = ""


def password_check(func):
    """ Check if the entered password is equal to the correct one,
        if it is, execute function 'func' """
    def func_wrapper(*args):
        if input("Please enter your password:\n") == my_password:
            func(*args)

    return func_wrapper


@password_check
def add_points(players, name, points):
    if name in players:
        players.get(name).points += points
    else:
        players[name] = Player(name, points)


@password_check
def reduce_points(players, number):
    """ Multiplies each player's score by number/100
        and rounds down to integer"""
    for player in players.values():
        player.points -= floor(player.points*number/100.0)


@password_check
def set_junior(players, name):
    players[name].junior = True


def print_players(players):
    """
    Sort players by points, reverse the order (more points => higher place),
    extract names of the players in sorted order and then print them
    """
    sorted_players = sorted(players, key=lambda x: x.points, reverse=True)
    for pos, player in enumerate(sorted_players, start=1):
        print("{0}. {1} - {2} pts".format(pos, player.name, player.points))


def ranking(players, junior):
    """ Print sorted all players, or juniors only if junior=True """
    if junior:
        print_players(list(filter(
            lambda x: x.junior, players.values())))
    else:
        print_players(players.values())


def main():
    global my_password
    my_password = str(input("Please set your password:\n"))
    players = {}

    while True:
        line = input().split()
        command = line[0]

        if command == "points":
            add_points(players, line[1], int(line[2]))
        elif command == "reduce":
            reduce_points(players, int(line[1]))
        elif command == "junior":
            set_junior(players, line[1])
        elif command == "ranking":
            # len(line)>1 indicates that 'junior' arg was passed
            ranking(players, len(line) > 1)
        elif command == "quit":
            password_check(sys.exit)()


if __name__ == "__main__":
    main()
